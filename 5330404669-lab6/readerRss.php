<?php

header('Content-type: text/html; charset=utf-8');

$url = "http://www.gotoknow.org/blogs/posts?format=rss";

$xml = file_get_contents($url);

$doc = new DOMDocument();

$doc->loadXML($xml);

$items = $doc->getElementsByTagName("item");

$i = 0;
while ($i < $items->length) {
    $i++;
    $item = $items->item($i);
    $titles = $item->getElementsByTagName("title");
    $links = $item->getElementsByTagname("link");
    $authors = $item->getElementsByTagname("author");

    echo "==>title" . "<br/>";
    echo "title = " . $titles->item(0)->nodeValue . "<br/>";
    echo "link = " . $links->item(0)->nodeValue . "<br/>";
    echo "author = " . $authors->item(0)->nodeValue . "<br/>";
    echo "==>/title" . "<br/>";
}
?>