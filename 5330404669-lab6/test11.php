<?php

//test11.php
// create new XMLWriter object
$writer = new XMLWriter();
// save XML file name as 'nation.xml'
$writer->openURI('nation.xml');
// write XML declaration
$writer->startDocument('1.0');
// write start tag for element 'nation'
$writer->startElement('nation');
// write attribute 'id' with its value 'th'
$writer->writeAttribute('id', 'th');
// write element 'name' with its value 'Thailand'
$writer->writeElement('name', 'Thailand');
// write element 'location' with its value 'Southeast Asia'
$writer->startElement('location');
$writer->writeAttribute('loc', 'asia');
$writer->writeRaw('Southeast Asia');
$writer->endElement(); // end element location
$writer->endElement(); // end element nation
$writer->endDocument();
$writer->flush();
?>