<?php

header('Content-type: text/html; charset=utf-8');
$url = "http://www.gotoknow.org/blogs/posts?format=rss";

$xml = file_get_contents($url);

$doc = new DOMDocument();
$xmlDoc = new DomDocument('1.0', 'UTF-8');
$doc->loadXML($xml);

echo "Reading from summaryBlogs.xml...<br/>";

$root = $xmlDoc->appendChild($xmlDoc->createElement('items'));
$items = $doc->getElementsByTagName("item");

for ($i = 0; $i < $items->length; $i++) {

    $item = $items->item($i);

    $titles = $item->getElementsByTagName("title");
    $links = $item->getElementsByTagname("link");
    $authors = $item->getElementsByTagname("author");


    echo $links->item(0)->nodeValue;
    echo $authors->item(0)->nodeValue;

    $titleData = $titles->item(0)->nodeValue;
    $linkData = $links->item(0)->nodeValue;
    $authorData = $authors->item(0)->nodeValue;

    $item = $root->appendChild($xmlDoc->createElement('item'));

    $title = $item->appendChild($xmlDoc->createElement('title'));

    $title->appendChild($xmlDoc->createTextNode($titleData));

    $link = $item->appendChild($xmlDoc->createElement('link'));

    $link->appendChild($xmlDoc->createTextNode($linkData));

    $author = $item->appendChild($xmlDoc->createElement('author'));

    $author->appendChild($xmlDoc->createTextNode($authorData));
}

$xmlDoc->save('summaryBlogs.xml'); // save
?>